(function(){
  if (document.querySelector(".app-search")) {
    return
  }
  const appContainer = document.createElement("div");
  appContainer.className = "app-searcher";
  const styles = document.createElement("style");
	styles.className = "app-searcher__styles"
  appContainer.innerHTML = 
  `<div id="app__container" class="app__container">
    <div class="app__drag-bar">
			<div id="app__containerheader" class="app__containerheader"> Click to drag </div>
			<button id="app__close-app" class="app__close-app">&times;</button>
    </div>
    <input class="app__input" type="text" placeholder="input CSS selectior"/>
    <button class="app__button app__search focus__btn" id="app__search" disabled>Search</button>
    <hr>
    <button class="app__button app__prev focus__btn" id="app__prev" disabled>Prev</button>
    <button class="app__button app__next focus__btn" id="app__next" disabled>Next</button>
    <button class="app__button app__child focus__btn" id="app__child" disabled>Child</button>
    <button class="app__button app__parent focus__btn" id="app__parent" disabled>Parent</button>
  </div>`;
  styles.innerText = `
  .app__button {
    border-radius: 3px;
    background-color:honeydew;
    padding:5px auto;
    margin: 5px;
    width: 60px;
    cursor: pointer;
    color: black;
  }
  .app__button:disabled {
  	cursor: default;
    color: grey;
  }
  .app__input {
    margin: 5px;
    padding: 5px;
    border-radius: 3px;
    width: 65%;
  }
  .app__close-app {
    cursor: pointer;
    font-size:20px;
    color: white;
    background-color:crimson;
    border: 2px solid red;
    display: flex;
    justify-content: flex-end;
  }
  .app__containerheader {
    height: 30px;
    cursor: move;
    background-color: #2196F3;
    text-align: center;
    z-index: 10;
    min-width: 90.7%;
  }
  .app__drag-bar {
    display: flex;
  }
  .app__container {
    position:absolute; 
    width: 300px;
    z-index:9;
    top:100px;
    right:50px;
    border: 3px solid gray; 
    border-radius: 5px; 
    background-color: AliceBlue;
  }
  .app__container--fixed {
    position: fixed;
  }`;
  document.head.appendChild(styles);
  document.body.appendChild(appContainer);
	
  const inputVal = document.querySelector(".app__input");
  const prev = document.querySelector(".app__prev");
  const next = document.querySelector(".app__next");
  const child = document.querySelector(".app__child");
  const parent = document.querySelector(".app__parent");
  const search = document.querySelector(".app__search");
  const btn = document.querySelector(".app__container");
  const closeBtn = document.querySelector(".app__close-app");
  const input = document.querySelector(".app__container");
  let borderColorBefore, elem, prevElement;
	
  const toggleBtn = (btn, value) => btn.disabled = value;
  const buttonControl = elem => {
    toggleBtn(prev, !elem.previousElementSibling);
    toggleBtn(next, !elem.nextElementSibling);
    toggleBtn(child, !elem.children[0]);
    toggleBtn(parent, !elem.parentElement);
  };
  const borderChange = elem => {
    if (prevElement) {
      prevElement.style.border = borderColorBefore;
    };
    prevElement = elem;
    if (elem) {
		  borderColorBefore = elem.style.border;
      elem.style.border = "3px solid red";
      buttonControl(elem);
    };
  };
  const handleClick = (e) => {
    const targetId = e.target.id;
    switch(targetId) {
      case "app__search": 
        elem = document.querySelector(inputVal.value);
        break;
      case "app__prev": 
        elem = elem.previousElementSibling;
        break;
      case "app__next": 
        elem = elem.nextElementSibling;
        break;
      case "app__child": 
        elem = elem.children[0];
        break;
      case "app__parent": 
        elem = elem.parentElement;
        break;
    };
    borderChange(elem);
  };
    const handleInput = e => toggleBtn(search, !e.target.value)
	const handleCloseBtn = e => {
	  btn.removeEventListener('click', handleClick);
  	closeBtn.removeEventListener('click', handleCloseBtn);
  	input.removeEventListener('input', handleInput);
		document.querySelector(".app-searcher__styles").remove()
		document.querySelector(".app-searcher").remove()
	}
  btn.addEventListener('click', handleClick);
  closeBtn.addEventListener('click', handleCloseBtn);
  input.addEventListener('input', handleInput);

dragElement(document.getElementById("app__container"));
  function dragElement(elmnt) {
    var pos1 = 0,
      pos2 = 0,
      pos3 = 0,
      pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {

      document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    } else {

      elmnt.onmousedown = dragMouseDown;
    };

    function dragMouseDown(e) {
      e = e || window.event;
      e.preventDefault();
      pos3 = e.clientX;
      pos4 = e.clientY;
      document.onmouseup = closeDragElement;
      document.onmousemove = elementDrag;
    };

    function elementDrag(e) {
      e = e || window.event;
      e.preventDefault();
      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;
      
      if (elmnt.offsetTop >= 0 ) {
        elmnt.style.top = elmnt.offsetTop - pos2 + "px";
      } else {
        elmnt.style.top = 1 + "px";
      }
      
      if (0 <= elmnt.offsetLeft) {
        elmnt.style.left = elmnt.offsetLeft - pos1 + "px";
      } else {
        elmnt.style.left = 1 + "px";
      }
      if (elmnt.offsetLeft <= document.documentElement.clientWidth - 300) {
        elmnt.style.left = document.documentElement.clientWidth - 300;
      } else {
        elmnt.style.left = document.documentElement.clientWidth - pos1 - 300  + "px";
      }
    };

    function closeDragElement() {

      document.onmouseup = null;
      document.onmousemove = null;
    };
  };
})();
