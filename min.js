(function(){if(document.querySelector(".app-search"))return;const a=document.createElement("div");a.className="app-searcher";const b=document.createElement("style");b.className="app-searcher__styles",a.innerHTML=`<div id="app__container" class="app__container">
    <div class="app__drag-bar">
			<div id="app__containerheader" class="app__containerheader"> Click to drag </div>
			<button id="app__close-app" class="app__close-app">&times;</button>
    </div>
    <input class="app__input" type="text" placeholder="input CSS selectior"/>
    <button class="app__button app__search focus__btn" id="app__search" disabled>Search</button>
    <hr>
    <button class="app__button app__prev focus__btn" id="app__prev" disabled>Prev</button>
    <button class="app__button app__next focus__btn" id="app__next" disabled>Next</button>
    <button class="app__button app__child focus__btn" id="app__child" disabled>Child</button>
    <button class="app__button app__parent focus__btn" id="app__parent" disabled>Parent</button>
  </div>`,b.innerText=`
  .app__button {
    border-radius: 3px;
    background-color:honeydew;
    padding:5px auto;
    margin: 5px;
    width: 60px;
    cursor: pointer;
    color: black;
  }
  .app__button:disabled {
  	cursor: default;
    color: grey;
  }
  .app__input {
    margin: 5px;
    padding: 5px;
    border-radius: 3px;
    width: 65%;
  }
  .app__close-app {
    cursor: pointer;
    font-size:20px;
    color: white;
    background-color:crimson;
    border: 2px solid red;
    display: flex;
    justify-content: flex-end;
  }
  .app__containerheader {
    height: 30px;
    cursor: move;
    background-color: #2196F3;
    text-align: center;
    z-index: 10;
    min-width: 90.7%;
  }
  .app__drag-bar {
    display: flex;
  }
  .app__container {
    position:absolute; 
    width: 300px;
    z-index:9;
    top:100px;
    right:50px;
    border: 3px solid gray; 
    border-radius: 5px; 
    background-color: AliceBlue;
  }
  .app__container--fixed {
    position: fixed;
  }`,document.head.appendChild(b),document.body.appendChild(a);const c=document.querySelector(".app__input"),d=document.querySelector(".app__prev"),e=document.querySelector(".app__next"),f=document.querySelector(".app__child"),g=document.querySelector(".app__parent"),h=document.querySelector(".app__search"),i=document.querySelector(".app__container"),j=document.querySelector(".app__close-app"),k=document.querySelector(".app__container");let l,m,n;const o=(a,b)=>a.disabled=b,p=a=>{o(d,!a.previousElementSibling),o(e,!a.nextElementSibling),o(f,!a.children[0]),o(g,!a.parentElement)},q=a=>{n&&(n.style.border=l);n=a,a&&(l=a.style.border,a.style.border="3px solid red",p(a))},r=a=>{const b=a.target.id;"app__search"===b?m=document.querySelector(c.value):"app__prev"===b?m=m.previousElementSibling:"app__next"===b?m=m.nextElementSibling:"app__child"===b?m=m.children[0]:"app__parent"===b?m=m.parentElement:void 0;q(m)},s=a=>o(h,!a.target.value),t=()=>{i.removeEventListener("click",r),j.removeEventListener("click",t),k.removeEventListener("input",s),document.querySelector(".app-searcher__styles").remove(),document.querySelector(".app-searcher").remove()};i.addEventListener("click",r),j.addEventListener("click",t),k.addEventListener("input",s),function(a){function b(a){a=a||window.event,a.preventDefault(),h=a.clientX,i=a.clientY,document.onmouseup=d,document.onmousemove=c}function c(b){b=b||window.event,b.preventDefault(),f=h-b.clientX,g=i-b.clientY,h=b.clientX,i=b.clientY,a.style.top=0<=a.offsetTop?a.offsetTop-g+"px":"1px",a.style.left=0<=a.offsetLeft?a.offsetLeft-f+"px":"1px",a.style.left=a.offsetLeft<=document.documentElement.clientWidth-300?document.documentElement.clientWidth-300:document.documentElement.clientWidth-f-300+"px"}function d(){document.onmouseup=null,document.onmousemove=null}var f=0,g=0,h=0,i=0;document.getElementById(a.id+"header")?document.getElementById(a.id+"header").onmousedown=b:a.onmousedown=b}(document.getElementById("app__container"))})();